const express = require('express');
const axios = require('axios');
const path = require('path');
const bodyParser = require('body-parser');

// This services URL
const HOST = '0.0.0.0';
const PORT = 3000;

// Service B's URL from environment variable set in docker-compose.yml
const REVERSER_SERVICE_URL = process.env.REVERSER_SERVICE_URL;

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));

// Serve the app UI
app.get('/', (request, response) => {
    response.sendFile(path.join(__dirname, '/index.html'));
});

// User input data
app.post('/process', (request, response) => {
    console.log(request.body);

    // Delegate processing of that data to service B
    // We can get away with doing this as part of the request as the service does not do all that much
    // but in most cases we would want a message queue in between to allow for asynchronity
    axios.post(`${REVERSER_SERVICE_URL}/process`, request.body)
    .then(resp => {

        // Data came back from processing
        response.send(resp.data.message);
    })
    .catch(error => console.log(error));
});

// This app is exposed to the host machine
app.listen(PORT, HOST, () => {
    console.log(`The server is running on http://${HOST}:${PORT}`);
});
