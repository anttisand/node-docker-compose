# A Simple Microservice Architecture using Docker and Docker Compose

In this example we use Docker Compose to compose an application consisting of two services, each contained in their own separate containers. These both services expose an API over HTTP to pass data around. Service A (```web_service/```) provides the UI, receives input from the user and delegates the processing of that input to Service B (```reverser_service/```) over HTTP.

```docker-compose.yml``` describes the application as a whole, the two services and the ports those use to communicate with each other and with the host. Actual instructions of what to do with each container is stored in a separete ```Dockerfile``` in the root of both services folders. These tell that container how to build and run that application, while ```docker-compose.yml``` holds the instruction for the application as a whole.

I've exposed the port 3000 from the UI service to the host machine, so that a user can access the UI, but the other services port 3030 is only exposed within the virtual network, that Docker Compose creates. This means, that Service A and Service B can talk to each other, the host can talk to Service A, but the host cannot talk to Service B directly. Instead, all messages must originate from Service A.

Docker Compose creates a default virtual network automatically. You can create more, if needed. Inside that network, we do not know what IP address Service B is going to get. That's why Service A cannot access Service B directly via an IP address. Luckily, Docker Compose can keep track of this for us. That's why we can simply use

```
environment:
      - REVERSER_SERVICE_URL=http://reverser:3030
```

as the Service B's URL in Service A. Here that 'reverser' part is the name of that service in the ```docker-compose.yml``` file.

## Installation and running instructions

Have Docker installed.

Navigate to the project root folder and run 

```
docker-compose up
```

to compose the individual Docker containers, compose the network, and run the services.

After this, you should be able to access the UI with a browser in ```http://0.0.0.0:3000```.

To bring down the containers, press ```Ctrl+C``` and execute

```
docker-compose down
```

to remove the containers and the network.

## Architectural notes

The web UI service is now making a direct point-to-point communication with the reverser service. We can get away with this, because we can assume, that the reverser service is able to answer to our requests in a reasonable time. However, for many application this kind of point-to-point communication might not be ideal. If the reverser service would be expriencing high load and could not answer in a reasonable time, the UI service would be also affected and exprience delays. This is usually managed by adding a message broker as an intermediary between the services and instead of making point-to-point communication, all services would conect only to the message broker instead. And that would, in turn, allow for asynchronous communication.

But to keep the example as simple as possible and because adding an intermediary message broker is the main topic of the group work exercise, this example architecture has a big design flaw. We can get away with it now, due to the very light-weight processing happening on the reverser service, but in many other more real-world use-cases, this would not be ideal.

## Update notes

15.2.2023
- Added ```.dorcerignore``` files so that the node_modules are not copied to the container. We get those by running the ```npm isntall``` command instead. This means, that Docker can cache those files.
- Added more comments to the Dockerfiles
