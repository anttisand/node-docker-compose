const express = require('express');
const bodyParser = require('body-parser');

// Receive the port to listen from docker-compose.yml
const PORT = process.env.PORT;

const app = express();
app.use(bodyParser.json());

// Accept data from Service A
app.post('/process', (request, response) => {
    console.log("Received: " + request.body.message);
    let message = request.body.message;

    // Process the data in some way
    message = message.split("").reverse().join("");
    console.log("Processed: " + message);

    // Send the processed data back to Service A
    response.json({message});
});

// This app is only exposed within the virtual network, we do not know it's IP address
app.listen(PORT, () => {
    console.log(`Service is running on port ${PORT}`);
});
